app.controller("ejercCtrl", function($scope, $http) {

	/**
	 *	Accionado por evento clic. Realiza peticion al Server.
	 */
	$scope.obtenerDatosServidorGET = function() {
		// Envio de datos al servidor por GET
		// TODO - 1.b. Implementar metodo $http.get()
	};
	
	/**
	 *	Accionado por evento clic. Realiza peticion al Server metodo POST.
	 */
	$scope.obtenerDatosServidorPOST = function() {
		// Envio de datos al servidor por POST
		// TODO - 1.c. Implementar metodo $http.post()
	};
	
});
