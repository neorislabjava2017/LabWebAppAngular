package com.businessMock;

import java.util.Map;
import java.util.logging.Logger;

import com.model.DatosPersona;

/**
 *	Mocks para emular consulta a la DB. 
 */
public class DatosPersonalesBOMock {
	
	private static final Logger LOGGER = Logger.getLogger("DatosPersonalesBOMock");
	
	/**
	 *	Mock para obtener los datos de una persona por datos de un mapa.
	 *	@param mapa
	 *	@return DatosPersona 
	 */
	public DatosPersona getDatosPesonaMock(Map<String, String[]> mapa) {		
		DatosPersona datosPersona = null;
		if (mapa != null && mapa.size() > 0) {
			for (String x : mapa.keySet()) {
				return this.getDatosPersonaMockById(mapa.get(x)[0]);
			}
		}
		return datosPersona;
	}
	
	/**
	 *	Mock para obtener los datos de una persona.
	 *	@param mapa
	 *	@return DatosPersona 
	 */
	public DatosPersona getDatosPersonaMockById(String id) {		
		DatosPersona datosPersona = null;
		if (id != null && !"".equals(id)) {
			LOGGER.info(" >>> LOG >>> " + id + " <<< LOG <<< ");
			switch (id) {
			case "1":
				datosPersona = new DatosPersona("Mario", "Flores", 40);
				break;
			case "2":
				datosPersona = new DatosPersona("Adrian", "Jimenez", 29);
				break;
			case "3":
				datosPersona = new DatosPersona("Beatriz", "Blanco", 26);
				break;
			case "4":
				datosPersona = new DatosPersona("Alejandro", "Garcia", 31);
				break;
			case "5":
				datosPersona = new DatosPersona("Patricia", "Gomez", 35);
				break;
			default:
				break;
			}
		}
		return datosPersona;
	}
	
}
