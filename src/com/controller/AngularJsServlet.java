package com.controller;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.constants.Constantes;
import com.model.DatosPersona;

/**
 *	Servlet utilizado para obtener datos del servidor. 
 *
	1.a. Completar layout como el indicado en la figura img/pantalla.png para representar datos devueltos
		 del servidor
	1.b. Implementar metodo $http.get() en controlador de AngularJS
	1.c. Implementar metodo $http.post() en controlador de AngularJS
	1.d. En el servlet, escribir headers del response para evitar caching del navegador web
	1.e. En el servlet, para el m�todo GET, Obtener mapa de par�metros provenientes del request 
	     y popular el objeto DatosPersona invocando al mock correspondiente.
	1.f. En el servlet, para el m�todo POST, popular el DTO ParamsEntradaDTO proveniente del request
		 y popular el objeto DatosPersona invocando al mock correspondiente.
	1.g. En el servlet, utilizar librer�a GSON para pasar el objeto obtenido en formato JSON 
		 para ambos tipos GET y POST
    1.h. Complementar los puntos b y c anteriores para manejo de error mostrando un pop-up alert indicando
         que se ha producido un error y borrando los datos - probarlo     
 *
 *
 */
// @WebServlet("/javaAngularJS/*")
public class AngularJsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger("AngularJsServlet");

	/**
	 *	Constructor no-arg. 
	 */
	public AngularJsServlet() {
		super();
	}

	/**
	 *	Procesamiento de la peticion enviada por el cliente.
	 *	@param request
	 *	@param response
	 *	@throws ServletException, IOException  
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		LOGGER.info("Inicio Service Servlet .... ");
		
		// TODO - 1.d. Escribir headers del response para evitar caching del navegador web
		
		DatosPersona datosPersona = null;
		// Metodo para obtener los datos del mock.
		switch (request.getMethod()) {
		case Constantes.METODO_HTTP_GET:
			// Se obtiene mapa del request para GET
			// TODO - 1.e. Para GET, obtener mapa de par�metros provenientes del request
			// y popular el objeto DatosPersona invocando al mock correspondiente.
			break;
		case Constantes.METODO_HTTP_POST:
			// Se obtiene DTO de entrada para POST
			// TODO - 1.f. Para POST, popular el DTO ParamsEntradaDTO proveniente del request
			// y popular el objeto DatosPersona invocando al mock correspondiente.
			break;
		}
		
		// TODO - 1.g. Utilizar librer�a GSON para pasar el objeto obtenido
		// en formato JSON para ambos tipos GET y POST
		
		LOGGER.info("Fin Service Servlet .... ");

	}

}
