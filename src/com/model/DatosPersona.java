package com.model;

/**
 *	Bean con representacion de datos de una persona. 
 */
public class DatosPersona {

	private String nombre;
	private String apellido;
	private int edad;
	
	/**
	 *	Constructor no-arg. 
	 */
	public DatosPersona() {
		super();
	}

	/**
	 *	Constructor args.
	 *	@param nombre
	 *	@param apellido
	 *	@param edad 
	 */
	public DatosPersona(String nombre, String apellido, int edad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
	}

	/**
	 *	@return nombre  
	 */
	public String getNombre() {
		return this.nombre;
	}

	/**
	 *	@param nombre 
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 *	@return apellido  
	 */
	public String getApellido() {
		return this.apellido;
	}

	/**
	 *	@param apellido 
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 *	@return edad  
	 */
	public int getEdad() {
		return this.edad;
	}

	/**
	 *	@param edad 
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}

}