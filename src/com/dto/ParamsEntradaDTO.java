package com.dto;

/**
 * 	DTO con parametros de entrada.
 */
public class ParamsEntradaDTO {
	
	private String id;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
